﻿using BestStudentsResults.DbTests.xUnit.Fixtures;
using BestStudentsResults.Services;
using BestStudentsResults.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BestStudentsResults.DbTests.xUnit
{
    public class RatingDbServiceMsSqlDbTests : IClassFixture<MsSqlDbFixture>
    {
        #region Configuration
        private readonly IRatingDbService _serviceUnderTest;
        private readonly MsSqlDbFixture _dbFixture;

        public RatingDbServiceMsSqlDbTests(MsSqlDbFixture dbFixture)
        {
            _serviceUnderTest = new RatingDbService(dbFixture.StudentResultDbContext);
            _dbFixture = dbFixture;
        }

        #endregion

        [Fact]
        public async Task AddRating_RecordShouldBeAddedToDb()
        {
            // Arrange
            var model = new StudentResultViewModel() { Rating = 5, StudentId = Guid.NewGuid() };

            //Act
            int result = await _serviceUnderTest.AddRatingAsync(model);

            //Assert
            Assert.NotEmpty(_dbFixture.StudentResultDbContext.StudentResults.ToList());
        }
    }
}