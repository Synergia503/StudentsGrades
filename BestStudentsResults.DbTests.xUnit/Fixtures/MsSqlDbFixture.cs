﻿using BestStudentsResults.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;

namespace BestStudentsResults.DbTests.xUnit.Fixtures
{
    public class MsSqlDbFixture : IDisposable
    {
        public MsSqlDbFixture()
        {
            DbContextOptions dbContextOptions = new DbContextOptionsBuilder()
                .UseSqlServer("Data Source=.;Initial Catalog=BestStudentResultsTests;Integrated Security=True;Persist Security Info=False;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False")
                .Options;
            StudentResultDbContext = new StudentResultDbContext(dbContextOptions);
            StudentResultDbContext.Database.EnsureCreated();
        }

        public StudentResultDbContext StudentResultDbContext { get; }

        public void Dispose()
        {
            StudentResultDbContext.Database.EnsureDeleted();
        }
    }
}