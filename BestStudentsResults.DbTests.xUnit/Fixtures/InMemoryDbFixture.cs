﻿using BestStudentsResults.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;

namespace BestStudentsResults.DbTests.xUnit.Fixtures
{
    public class InMemoryDbFixture : IDisposable
    {
        public InMemoryDbFixture()
        {
            DbContextOptions options = new DbContextOptionsBuilder()
                .UseInMemoryDatabase("InMemory")
                .Options;

            StudentResultDbContext = new StudentResultDbContext(options);
            StudentResultDbContext.Database.EnsureCreated();
        }

        public StudentResultDbContext StudentResultDbContext { get; }

        public void Dispose()
        {
            StudentResultDbContext.Database.EnsureDeleted();
        }
    }
}