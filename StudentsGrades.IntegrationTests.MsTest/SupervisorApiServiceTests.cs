﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentsGrades.Services;
using System;
using System.Threading.Tasks;

namespace StudentsGrades.IntegrationTests.MsTest
{
    [TestClass]
    public class SupervisorApiServiceTests
    {
        #region Configuration
        private static ISupervisorApiService _serviceUnderTest;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _serviceUnderTest = new SupervisorApiService();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            _serviceUnderTest.Dispose();
        }

        #endregion

        [TestMethod]
        public async Task SendRating_ShouldReturnTrue()
        {
            //Arrange
            var studentId = Guid.NewGuid();
            var rating = 5;

            //Act
            bool result = await _serviceUnderTest.SendRating(studentId, rating);

            //Assert
            Assert.IsTrue(result);
        }
    }
}