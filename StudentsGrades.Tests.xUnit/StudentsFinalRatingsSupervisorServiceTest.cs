﻿using Moq;
using StudentsGrades.Models;
using StudentsGrades.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace StudentsGrades.Tests.xUnit
{
    public class StudentsFinalRatingsSupervisorServiceTest
    {
        #region Configuration
        private readonly IStudentsFinalRatingsSupervisorService _serviceUnderTest;
        private readonly Mock<ISupervisorApiService> _supervisorApiServiceMock;
        private readonly Mock<IStudentsFinalRatingsService> _studentsFinalRatingsServiceMock;
        public StudentsFinalRatingsSupervisorServiceTest()
        {
            _supervisorApiServiceMock = new Mock<ISupervisorApiService>();
            _studentsFinalRatingsServiceMock = new Mock<IStudentsFinalRatingsService>();
            _serviceUnderTest = new StudentsFinalRatingsSupervisorService(_studentsFinalRatingsServiceMock.Object, _supervisorApiServiceMock.Object);
        }
        #endregion

        [Fact]
        public async Task GetFinalRating_RatingIsLessThan4_RequestToSupervisorShouldNotBeSent()
        {
            //Arrange
            Guid studentId = Guid.NewGuid();
            var grades = new List<Grade>();
            _studentsFinalRatingsServiceMock.Setup(s => s.GetFinalRating(grades))
                .Returns(3);
            //Act
            int result = await _serviceUnderTest.GetFinalRating(studentId, grades);

            //Assert
            _supervisorApiServiceMock.Verify(s => s.SendRating(studentId, result), Times.Never);
        }

        [Fact]
        public async Task GetFinalRating_RatingIsEqualTo4_RequestToSupervisorShouldBeSent()
        {
            //Arrange
            Guid studentId = Guid.NewGuid();
            var grades = new List<Grade>();
            _studentsFinalRatingsServiceMock.Setup(s => s.GetFinalRating(grades))
                .Returns(4);
            //Act
            int result = await _serviceUnderTest.GetFinalRating(studentId, grades);

            //Assert
            _supervisorApiServiceMock.Verify(s => s.SendRating(studentId, result), Times.Once);
        }

        [Fact]
        public async Task GetFinalRating_RatingIsGreaterThan4_RequestToSupervisorShouldBeSent()
        {
            //Arrange
            Guid studentId = Guid.NewGuid();
            var grades = new List<Grade>();
            _studentsFinalRatingsServiceMock.Setup(s => s.GetFinalRating(grades))
                .Returns(5);
            //Act
            int result = await _serviceUnderTest.GetFinalRating(studentId, grades);

            //Assert
            _supervisorApiServiceMock.Verify(s => s.SendRating(studentId, result), Times.Once);
        }
    }
}