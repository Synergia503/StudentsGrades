﻿using StudentsGrades.Models;
using System.Collections;
using System.Collections.Generic;

namespace StudentsGrades.Tests.xUnit.ClassData
{
    public class StudentGradesServiceTestsClassData : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>()
        {
            new object[]
            {
                new List<Grade>()
                {
                    new Grade(){ Value = 2.75m, Weight = 1 },
                    new Grade(){ Value = 1.75m, Weight = 1 },
                    new Grade(){ Value = 2.75m, Weight = 1 },
                },
                2.4167m
            },
            new object[]
            {
                new List<Grade>()
                {
                    new Grade(){ Value = 2.75m, Weight = 1 },
                    new Grade(){ Value = 1.75m, Weight = 1 },
                    new Grade(){ Value = 1.75m, Weight = 1 },
                },
                2.0833m
            }
        };

        public IEnumerator<object[]> GetEnumerator()
            => data.GetEnumerator();


        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();
    }
}