using StudentsGrades.Models;
using StudentsGrades.Services;
using StudentsGrades.Tests.xUnit.ClassData;
using System.Collections.Generic;
using Xunit;

namespace StudentsGrades.Tests.xUnit
{
    public class StudentsGradesServiceTests
    {
        #region Configuration
        StudentGradesAverageService serviceUnderTests;
        public StudentsGradesServiceTests()
        {
            serviceUnderTests = new StudentGradesAverageService();
        }
        #endregion

        [Fact]
        public void Calculate_ShouldReturnCorrectResult()
        {
            // Arrange
            var list = new List<Grade>()
            {
                new Grade() { Value = 5, Weight = 2},
                new Grade() { Value = 4, Weight = 3}
            };

            //Act
            var result = serviceUnderTests.Calculate(list);

            //Assert
            Assert.Equal(4.4m, result);
        }

        [Theory]
        [ClassData(typeof(StudentGradesServiceTestsClassData))]
        public void Calculate_ShouldReturnCorrectResults(List<Grade> grades, decimal expectedResult)
        {
            // Arrange

            //Act
            var result = serviceUnderTests.Calculate(grades);

            //Assert
            Assert.Equal(expectedResult, result);
        }
    }
}