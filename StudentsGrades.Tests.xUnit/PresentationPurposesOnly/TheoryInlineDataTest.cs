﻿using Xunit;

namespace StudentsGrades.Tests.xUnit.PresentationPurposesOnly
{
    public class TheoryInlineDataTest
    {
        [Theory]
        [InlineData(1, 2)]
        [InlineData(3, 4)]
        [InlineData(5, 6)]
        public void InlineDataTest(int a, int b)
        {
            Assert.NotEqual(a, b);
        }
    }
}