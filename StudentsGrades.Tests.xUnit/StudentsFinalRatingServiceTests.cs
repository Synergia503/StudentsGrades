﻿using Moq;
using StudentsGrades.Services;
using Xunit;

namespace StudentsGrades.Tests.xUnit
{
    public class StudentsFinalRatingServiceTests
    {
        #region Configuration
        private readonly StudentsFinalRatingsService _serviceUnderTests;
        private readonly Mock<IStudentGradesAverageService> _studentsGradesAverageServiceMock;

        public StudentsFinalRatingServiceTests()
        {
            _studentsGradesAverageServiceMock = new Mock<IStudentGradesAverageService>();
            _serviceUnderTests = new StudentsFinalRatingsService(_studentsGradesAverageServiceMock.Object);
        }
        #endregion

        [Theory]
        [InlineData(5.5, 6)]
        [InlineData(4.75, 5)]
        [InlineData(3.75, 4)]
        [InlineData(2.75, 3)]
        [InlineData(1.75, 2)]
        [InlineData(1.65, 1)]
        public void GetFinalRating_ShouldReturnCorrectResult(double average, int expectedResult)
        {
            //Arrange
            _studentsGradesAverageServiceMock.Setup(s => s.Calculate(null)).Returns((decimal)average);

            //Act
            var result = _serviceUnderTests.GetFinalRating(null);

            //Assert
            Assert.Equal(expectedResult, result);
        }
    }
}