﻿using StudentsGrades.IntegrationTests.xUnit.Fixtures;
using System;
using System.Threading.Tasks;
using Xunit;

namespace StudentsGrades.IntegrationTests.xUnit
{
    public class SupervisorApiServiceTests : IClassFixture<SupervisorApiServiceFixture>
    {
        private readonly SupervisorApiServiceFixture _fixture;

        public SupervisorApiServiceTests(SupervisorApiServiceFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task SendRatingWithGuid_ApiShouldReturnTrue()
        {
            // Arrange
            var studentId = Guid.NewGuid();
            var rating = 4;

            //Act
            bool result = await _fixture.ServiceUnderTest.SendRating(studentId, rating);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public async Task SendRatingWithEmptyGuid_ApiShouldReturnFalse()
        {
            // Arrange
            var studentId = Guid.Empty;
            var rating = 4;

            //Act
            bool result = await _fixture.ServiceUnderTest.SendRating(studentId, rating);

            //Assert
            Assert.False(result);
        }
    }
}