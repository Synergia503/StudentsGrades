﻿using StudentsGrades.Services;
using System;

namespace StudentsGrades.IntegrationTests.xUnit.Fixtures
{
    public class SupervisorApiServiceFixture : IDisposable
    {
        public SupervisorApiServiceFixture()
        {
            ServiceUnderTest = new SupervisorApiService();
        }

        public SupervisorApiService ServiceUnderTest { get; }

        public void Dispose()
        {
            ServiceUnderTest.Dispose();
        }
    }
}