﻿using NUnit.Framework;

namespace StudentsGrades.Tests.nUnit.PresentationPurposesOnly
{
    public class TestCaseExample
    {
        [Test]
        [TestCase(1, 2)]
        [TestCase(3, 4)]
        [TestCase(5, 6)]
        public void TestCaseTest(int a, int b)
        {
            Assert.AreNotEqual(a, b);
        }
    }
}