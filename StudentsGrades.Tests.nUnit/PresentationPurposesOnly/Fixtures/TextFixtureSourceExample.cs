﻿using NUnit.Framework;

namespace StudentsGrades.Tests.nUnit.PresentationPurposesOnly.Fixtures
{
    [TestFixtureSource(typeof(TestFixtureDataExampleSource), nameof(TestFixtureDataExampleSource.Data))]
    public class TextFixtureSourceExample
    {
        private readonly int _a;
        private readonly int _b;

        public TextFixtureSourceExample(int a, int b)
        {
            _a = a;
            _b = b;
        }

        [Test]
        public void AreNotEqual()
        {
            Assert.AreNotEqual(_a, _b);
        }

        [Test]
        public void BIsGreaterThanA()
        {
            Assert.Greater(_b, _a);
        }
    }
}