﻿using NUnit.Framework;
using System.Collections;

namespace StudentsGrades.Tests.nUnit.PresentationPurposesOnly.Fixtures
{
    public class TestFixtureDataExampleSource
    {
        public static IEnumerable Data
        {
            get
            {
                yield return new TestFixtureData(1, 2);
            }
        }
    }
}