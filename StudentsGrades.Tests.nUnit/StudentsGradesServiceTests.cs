﻿using NUnit.Framework;
using StudentsGrades.Models;
using StudentsGrades.Services;
using StudentsGrades.Tests.nUnit.TestCaseSource.StudentsGradesServiceTests;
using System.Collections;
using System.Collections.Generic;

namespace StudentsGrades.Tests.nUnit
{
    [TestFixture]
    public class StudentsGradesServiceTests
    {
        #region Configuration
        StudentGradesAverageService serviceUnderTests;
        public StudentsGradesServiceTests()
        {
            serviceUnderTests = new StudentGradesAverageService();
        }

        public static IEnumerable GradesLists
        {
            get
            {
                yield return new TestCaseData(new List<Grade>()
                {
                    new Grade(){ Value = 2.75m, Weight = 1 },
                    new Grade(){ Value = 1.75m, Weight = 1 },
                    new Grade(){ Value = 2.75m, Weight = 1 },
                },
                2.4167m);
                yield return new TestCaseData(new List<Grade>()
                {
                    new Grade(){ Value = 2.75m, Weight = 1 },
                    new Grade(){ Value = 1.75m, Weight = 1 },
                    new Grade(){ Value = 1.75m, Weight = 1 },
                },
                2.0833m);
            }
        }
        #endregion

        [Test]
        [TestCaseSource(typeof(StudentsGradesServiceTests), nameof(GradesLists))]
        public void Calculate_ShouldReturnCorrectResults(List<Grade> grades, decimal expectedResult)
        {
            //Act
            var result = serviceUnderTests.Calculate(grades);

            //Assert
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        [TestCaseSource(typeof(StudentsGradesServiceTestsTestCaseSource), nameof(GradesLists))]
        public void Calculate_ShouldReturnCorrectResultsWithTestCaseSourceFromOutsideClass(List<Grade> grades, decimal expectedResult)
        {
            //Act
            var result = serviceUnderTests.Calculate(grades);

            //Assert
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void Calculate_ShouldReturnCorrectResult()
        {
            // Arrange
            var list = new List<Grade>()
            {
                new Grade() { Value = 5, Weight = 2},
                new Grade() { Value = 4, Weight = 3}
            };

            //Act
            var result = serviceUnderTests.Calculate(list);

            //Assert
            Assert.AreEqual(4.4m, result);
        }
    }
}