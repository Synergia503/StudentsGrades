﻿using NSubstitute;
using NUnit.Framework;
using StudentsGrades.Services;

namespace StudentsGrades.Tests.nUnit
{
    [TestFixture]
    public class StudentsFinalRatingServiceTests
    {
        #region Configuration
        private readonly StudentsFinalRatingsService _serviceUnderTests;
        private readonly IStudentGradesAverageService _studentsGradesAverageServiceMock;

        public StudentsFinalRatingServiceTests()
        {
            _studentsGradesAverageServiceMock = Substitute.For<IStudentGradesAverageService>();
            _serviceUnderTests = new StudentsFinalRatingsService(_studentsGradesAverageServiceMock);
        }
        #endregion

        [Test]
        [TestCase(5.5, 6)]
        [TestCase(4.75, 5)]
        [TestCase(3.75, 4)]
        [TestCase(2.75, 3)]
        [TestCase(1.75, 2)]
        [TestCase(1.65, 1)]
        public void GetFinalRating_ShouldReturnCorrectResult(double average, int expectedResult)
        {
            //Arrange            
            _studentsGradesAverageServiceMock.Calculate(null).Returns((decimal)average);

            //Act
            var result = _serviceUnderTests.GetFinalRating(null);

            //Assert
            Assert.AreEqual(expectedResult, result);
        }
    }
}