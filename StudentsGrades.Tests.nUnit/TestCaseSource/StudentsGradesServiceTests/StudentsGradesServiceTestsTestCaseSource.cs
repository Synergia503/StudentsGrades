﻿using NUnit.Framework;
using StudentsGrades.Models;
using System.Collections;
using System.Collections.Generic;

namespace StudentsGrades.Tests.nUnit.TestCaseSource.StudentsGradesServiceTests
{
    public class StudentsGradesServiceTestsTestCaseSource
    {
        public static IEnumerable GradesLists
        {
            get
            {
                yield return new TestCaseData(new List<Grade>()
                {
                    new Grade(){ Value = 2.75m, Weight = 1 },
                    new Grade(){ Value = 1.75m, Weight = 1 },
                    new Grade(){ Value = 2.75m, Weight = 1 },
                },
                2.4167m);
                yield return new TestCaseData(new List<Grade>()
                {
                    new Grade(){ Value = 2.75m, Weight = 1 },
                    new Grade(){ Value = 1.75m, Weight = 1 },
                    new Grade(){ Value = 1.75m, Weight = 1 },
                },
                2.0833m);
            }
        }
    }
}