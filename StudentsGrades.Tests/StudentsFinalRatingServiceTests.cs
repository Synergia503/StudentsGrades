﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentsGrades.Models;
using StudentsGrades.Services;
using System.Collections.Generic;

namespace StudentsGrades.Tests
{
    [TestClass]
    public class StudentsFinalRatingServiceTests
    {
        [DataTestMethod]
        [DataRow(5.5, 6)]
        [DataRow(4.75, 5)]
        [DataRow(3.75, 4)]
        [DataRow(2.75, 3)]
        [DataRow(1.75, 2)]
        [DataRow(1.65, 1)]
        public void GetFinalRating_ShouldReturnCorrectResult(double average, int expectedResult)
        {
            //Arrange
            IStudentGradesAverageService studentsGradesAverageService = new StudentGradesAverageServiceMock((decimal)average);
            var serviceUnderTesting = new StudentsFinalRatingsService(studentsGradesAverageService);

            //Act
            int result = serviceUnderTesting.GetFinalRating(null);

            //Assert
            Assert.AreEqual(expectedResult, result);
        }
    }

    public class StudentGradesAverageServiceMock : IStudentGradesAverageService
    {
        private readonly decimal _average;

        public StudentGradesAverageServiceMock(decimal average)
        {
            _average = average;
        }

        public decimal Calculate(IEnumerable<Grade> grades)
        {
            return _average;
        }
    }
}