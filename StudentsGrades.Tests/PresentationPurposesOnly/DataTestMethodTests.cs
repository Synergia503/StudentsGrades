﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StudentsGrades.Tests.PresentationPurposesOnly
{
    [TestClass]
    public class DataTestMethodTests
    {
        [DataTestMethod]
        [DataRow(1, 2)]
        [DataRow(3, 4)]
        [DataRow(5, 6)]
        public void DataRowExample(int a, int b)
        {
            Assert.AreNotEqual(a, b);
        }
    }
}