﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace StudentsGrades.Tests.IntegrationsTests
{
    [TestClass]
    public class IoCIntegrationTest
    {
        #region Configuration
        private readonly IContainer _container;

        public IoCIntegrationTest()
        {
            var builder = new ContainerBuilder();
            Assembly studentsGradesAssembly = Assembly.Load("StudentsGrades");
            builder.RegisterAssemblyTypes(studentsGradesAssembly)
                .Where(s => s.Name.EndsWith("Service"))
                .AsImplementedInterfaces();
            _container = builder.Build();
        }

        #endregion

        [TestMethod]
        public void ContainerResolve_AllTypesShouldBeRegistered()
        {
            //Arrange
            Assembly studentsGradesAssembly = Assembly.Load("StudentsGrades");
            IEnumerable<Type> typesToResolve = studentsGradesAssembly.GetTypes()
                    .Where(t => t.Name.EndsWith("Service") && t.IsInterface);
            var typeObjects = new List<object>();

            //Act
            foreach (Type typeToResolve in typesToResolve)
            {
                typeObjects.Add(_container.Resolve(typeToResolve));
            }

            //Assert
            Assert.AreEqual(typesToResolve.Count(), typeObjects.Count);

        }
    }
}