﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BestStudentsResults.Data.Entity
{
    public class StudentResult
    {
        [Key]
        public Guid Id { get; set; }
        public Guid StudentId { get; set; }
        public int Rating { get; set; }
    }
}