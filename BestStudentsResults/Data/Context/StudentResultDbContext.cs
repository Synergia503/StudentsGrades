﻿using BestStudentsResults.Data.Entity;
using Microsoft.EntityFrameworkCore;

namespace BestStudentsResults.Data.Context
{
    public class StudentResultDbContext : DbContext
    {
        public StudentResultDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<StudentResult> StudentResults { get; set; }
    }
}