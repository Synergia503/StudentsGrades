﻿using Microsoft.AspNetCore.Mvc;

namespace BestStudentsResults.Controllers
{
    [Route("api/apiState")]
    public class ApiStateController : Controller
    {
        [HttpGet("check")]
        public IActionResult Check()
        {
            return Json("Api is now ready to very hard work.");
        }
    }
}