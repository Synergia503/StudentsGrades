﻿using BestStudentsResults.Services;
using BestStudentsResults.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace BestStudentsResults.Controllers
{
    [Route("api/result")]
    public class ResultController : Controller
    {
        private readonly IRatingDbService _ratingDbService;

        public ResultController(IRatingDbService ratingDbService)
        {
            _ratingDbService = ratingDbService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]StudentResultViewModel model)
        {
            // Bussiness logic here for presentation purposes only
            if (model.StudentId == Guid.Empty)
            {
                return Json(false);
            }

            await _ratingDbService.AddRatingAsync(model);
            return Json(true);
        }
    }
}