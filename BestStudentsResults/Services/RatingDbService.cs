﻿using BestStudentsResults.Data.Context;
using BestStudentsResults.Data.Entity;
using BestStudentsResults.ViewModels;
using System.Threading.Tasks;

namespace BestStudentsResults.Services
{
    public class RatingDbService : IRatingDbService
    {
        private readonly StudentResultDbContext _dbContext;

        public RatingDbService(StudentResultDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int> AddRatingAsync(StudentResultViewModel model)
        {
            var studentResultEntity = new StudentResult { StudentId = model.StudentId, Rating = model.Rating };
            _dbContext.StudentResults.Add(studentResultEntity);
            return await _dbContext.SaveChangesAsync();
        }
    }
}