﻿using BestStudentsResults.ViewModels;
using System.Threading.Tasks;

namespace BestStudentsResults.Services
{
    public interface IRatingDbService
    {
        Task<int> AddRatingAsync(StudentResultViewModel model);
    }
}