﻿using NUnit.Framework;
using StudentsGrades.Services;
using System;
using System.Threading.Tasks;

namespace StudentsGrades.IntegrationTests.nUnit
{
    [TestFixture]
    public class SupervisorApiServiceTests
    {
        #region Configuration
        private ISupervisorApiService _serviceUnderTest;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _serviceUnderTest = new SupervisorApiService();
        }

        [OneTimeTearDown]
        public void Cleanup()
        {
            _serviceUnderTest.Dispose();
        }
        #endregion

        [Test]
        public async Task SendRating_ShouldReturnTrue()
        {
            //Arrange
            var studentId = Guid.NewGuid();
            var rating = 3;

            //Act
            bool result = await _serviceUnderTest.SendRating(studentId, rating);

            //Assert
            Assert.True(result);
        }
    }
}