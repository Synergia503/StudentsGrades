﻿using StudentsGrades.Models;
using StudentsGrades.Services;
using System;
using System.Collections.Generic;

//Such console apps shouldn't be in main solution, but this one is only for presentation purposes
namespace ConsoleAppForTesting
{
    class Program
    {
        static void Main(string[] args)
        {
            var gradesList = new List<Grade>()
            {
                new Grade() { Value = 5, Weight = 2 },
                new Grade() { Value = 4, Weight = 3 }
            };

            var studentsGradesService = new StudentGradesAverageService();
            var result = studentsGradesService.Calculate(gradesList);
            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}