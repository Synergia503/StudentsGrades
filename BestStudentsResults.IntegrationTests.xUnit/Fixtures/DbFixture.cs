﻿using BestStudentsResults.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;

namespace BestStudentsResults.IntegrationTests.xUnit.Fixtures
{
    public class DbFixture : IDisposable
    {
        public DbFixture()
        {
            DbContextOptions dbContextOptions = new DbContextOptionsBuilder()
                .UseSqlServer("Data Source=.;Initial Catalog=BestStudentResults;Integrated Security=True;Persist Security Info=False;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False")
                .Options;
            StudentResultDbContext = new StudentResultDbContext(dbContextOptions);
            StudentResultDbContext.Database.EnsureCreated();
        }

        public StudentResultDbContext StudentResultDbContext { get; }

        public void Dispose()
        {            
            StudentResultDbContext.Dispose();
        }
    }
}