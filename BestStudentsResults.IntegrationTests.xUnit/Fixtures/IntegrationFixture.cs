﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration.Json;
using System;
using System.Net.Http;

namespace BestStudentsResults.IntegrationTests.xUnit.Fixtures
{
    public class IntegrationFixture : IDisposable
    {
        public IntegrationFixture()
        {
            var jsonConfigurationSource = new JsonConfigurationSource { Path = "appsettings.json" };
            _server = new TestServer(
                new WebHostBuilder()
                .UseStartup<Startup>()
                .ConfigureAppConfiguration(c => c.Add(jsonConfigurationSource)));
            Client = _server.CreateClient();
        }

        private readonly TestServer _server;
        public HttpClient Client { get; }

        public void Dispose()
        {
            Client.Dispose();
            _server.Dispose();
        }
    }
}