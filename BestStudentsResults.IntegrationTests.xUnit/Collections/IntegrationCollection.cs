﻿using BestStudentsResults.IntegrationTests.xUnit.Fixtures;
using Xunit;

namespace BestStudentsResults.IntegrationTests.xUnit.Collections
{
    [CollectionDefinition(nameof(IntegrationCollection))]
    public class IntegrationCollection : ICollectionFixture<IntegrationFixture>, ICollectionFixture<DbFixture>
    {
    }
}