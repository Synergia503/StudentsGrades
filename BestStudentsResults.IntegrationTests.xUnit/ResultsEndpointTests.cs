using BestStudentsResults.IntegrationTests.xUnit.Collections;
using BestStudentsResults.IntegrationTests.xUnit.Fixtures;
using BestStudentsResults.ViewModels;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BestStudentsResults.IntegrationTests.xUnit
{
    [Collection(nameof(IntegrationCollection))]
    public class ResultsEndpointTests
    {
        #region Configuration
        private readonly IntegrationFixture _integrationFixture;
        private readonly DbFixture _dbFixture;
        private readonly string endpoint = "api/result";

        public ResultsEndpointTests(IntegrationFixture integrationFixture, DbFixture dbFixture)
        {
            _integrationFixture = integrationFixture;
            _dbFixture = dbFixture;
        }
        #endregion
        [Fact]
        public async Task Post_ResultShouldNotBeSavedInDb_PassedGuidIsEmpty()
        {
            //Arrange
            var model = new StudentResultViewModel() { StudentId = Guid.Empty, Rating = 3 };

            //Act
            HttpResponseMessage response = await _integrationFixture
                .Client
                .PostAsync(
                    endpoint,
                    new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));

            //Assert
            Assert.True(response.IsSuccessStatusCode);            
            Assert.Equal(0, _dbFixture.StudentResultDbContext.StudentResults.Where(r => r.StudentId == model.StudentId).Count());
        }

        [Fact]
        public async Task Post_ResultShouldBeSavedInDb()
        {
            //Arrange
            var model = new StudentResultViewModel() { StudentId = Guid.NewGuid(), Rating = 6 };

            //Act
            HttpResponseMessage response = await _integrationFixture
                .Client
                .PostAsync(
                    endpoint,
                    new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));

            //Assert
            Assert.True(response.IsSuccessStatusCode);
            Assert.Equal(1, _dbFixture.StudentResultDbContext.StudentResults.Where(r => r.StudentId == model.StudentId).Count());
        }
    }
}