﻿using Autofac;
using StudentsGrades.Services;
using System.Reflection;

namespace IoCExampleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ContainerBuilder();

            Assembly studentsGradesAssembly = Assembly.Load("StudentsGrades");
            builder.RegisterAssemblyTypes(studentsGradesAssembly)
                .Where(s => s.Name.EndsWith("Service"))
                .AsImplementedInterfaces();

            IContainer container = builder.Build();
            var studentsFinalRatingsService = container.Resolve<IStudentsFinalRatingsService>();
        }
    }
}