﻿using StudentsGrades.Consts;
using StudentsGrades.Models;
using System.Collections.Generic;

namespace StudentsGrades.Services
{
    public class StudentsFinalRatingsService : IStudentsFinalRatingsService
    {
        private readonly IStudentGradesAverageService _studentGradesAverageService;

        public StudentsFinalRatingsService(IStudentGradesAverageService studentGradesAverageService)
        {
            _studentGradesAverageService = studentGradesAverageService;
        }

        public int GetFinalRating(List<Grade> grades)
        {
            decimal average = _studentGradesAverageService.Calculate(grades);
            if (average >= RatingsSteps.Rating6)
            {
                return 6;
            }
            else if (average >= RatingsSteps.Rating5)
            {
                return 5;
            }
            else if (average >= RatingsSteps.Rating4)
            {
                return 4;
            }
            else if (average >= RatingsSteps.Rating3)
            {
                return 3;
            }
            else if (average >= RatingsSteps.Rating2)
            {
                return 2;
            }
            else
            {
                return 1;
            }
        }
    }
}