﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace StudentsGrades.Services
{
    public class SupervisorApiService : ISupervisorApiService
    {
        private readonly HttpClient _client;
        public SupervisorApiService()
        {
            _client = new HttpClient { BaseAddress = new Uri("http://beststudentsresultsapi.localtest.me:90/api/") };
        }

        public async Task<bool> SendRating(Guid studentId, int rating)
        {
            var model = new { StudentId = studentId, Rating = rating };

            HttpResponseMessage response = await _client.PostAsync("result", new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
            if (response.IsSuccessStatusCode)
            {
                string responseContent = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<bool>(responseContent);
            }

            return false;
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}