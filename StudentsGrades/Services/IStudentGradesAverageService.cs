﻿using StudentsGrades.Models;
using System.Collections.Generic;

namespace StudentsGrades.Services
{
    public interface IStudentGradesAverageService
    {
        decimal Calculate(IEnumerable<Grade> grades);
    }
}