﻿using StudentsGrades.Models;
using System.Collections.Generic;

namespace StudentsGrades.Services
{
    public interface IStudentsFinalRatingsService
    {
        int GetFinalRating(List<Grade> grades);
    }
}