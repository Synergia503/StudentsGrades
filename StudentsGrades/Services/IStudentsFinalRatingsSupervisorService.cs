﻿using StudentsGrades.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudentsGrades.Services
{
    public interface IStudentsFinalRatingsSupervisorService
    {
        Task<int> GetFinalRating(Guid studentId, List<Grade> grades);
    }
}