﻿using StudentsGrades.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudentsGrades.Services
{
    public class StudentGradesAverageService : IStudentGradesAverageService
    {
        public decimal Calculate(IEnumerable<Grade> grades)
        {
            decimal gradesSum = grades.Sum(g => g.Value * g.Weight);
            return Math.Round(gradesSum / grades.Sum(g => g.Weight), 4);
        }
    }
}