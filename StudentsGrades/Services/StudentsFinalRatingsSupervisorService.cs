﻿using StudentsGrades.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudentsGrades.Services
{
    public class StudentsFinalRatingsSupervisorService : IStudentsFinalRatingsSupervisorService
    {
        private readonly IStudentsFinalRatingsService _studentsFinalRatingsService;
        private readonly ISupervisorApiService _supervisorApiService;

        public StudentsFinalRatingsSupervisorService(IStudentsFinalRatingsService studentsFinalRatingsService, ISupervisorApiService supervisorApiService)
        {
            _studentsFinalRatingsService = studentsFinalRatingsService;
            _supervisorApiService = supervisorApiService;
        }

        public async Task<int> GetFinalRating(Guid studentId, List<Grade> grades)
        {
            int rating = _studentsFinalRatingsService.GetFinalRating(grades);
            if (rating >= 4)
            {
                await _supervisorApiService.SendRating(studentId, rating);
            }

            return rating;
        }
    }
}