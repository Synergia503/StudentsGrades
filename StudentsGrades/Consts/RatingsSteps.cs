﻿namespace StudentsGrades.Consts
{
    public class RatingsSteps
    {
        public const decimal Rating6 = 5.5m;
        public const decimal Rating5 = 4.75m;
        public const decimal Rating4 = 3.75m;
        public const decimal Rating3 = 2.75m;
        public const decimal Rating2 = 1.7m;        
    }
}